<?php
	 
	add_action( 'after_setup_theme', 'wedo_theme_setup' );
	function wedo_theme_setup() {
		add_theme_support( 'automatic-feed-links' );
		add_theme_support( 'post-thumbnails' );
		add_action( 'widgets_init', 'wedo_register_sidebars' );
		add_filter('wp_mail_from_name', 'new_mail_from_name');
		add_action( 'init', 'register_my_menus' );
		add_action( 'login_enqueue_scripts', 'my_login_logo' );
		add_action('login_head', 'add_favicon');
		add_action('admin_head', 'add_favicon');
		add_action('init', 'wedo_posts');
		add_action('init', 'wedo_taxonomies');
		add_action('init', 'flush_rewrite_rules');
		add_action( 'wp_enqueue_scripts', 'wedo_scripts' );
		add_action('wp_before_admin_bar_render', 'annointed_admin_bar_remove', 0);
		add_filter( 'wpseo_metabox_prio', function() { return 'low';});
		add_image_size( 'small-square', 200, 200, true );
		add_image_size( 'square', 400, 400, true );
		add_image_size( 'post-feature', 900, 450, true );
	}

	get_template_part('functions/include', 'favicons');
	get_template_part('functions/include', 'postnav');
	get_template_part('functions/include', 'menus');
	get_template_part('functions/include', 'scripts');
	get_template_part('functions/include', 'sidebar');
	get_template_part('functions/include', 'cpts');
	get_template_part('functions/include', 'users');
	get_template_part('functions/include', 'email');
	get_template_part('functions/include', 'footer');
	get_template_part('functions/include', 'gallery'); 
	get_template_part('functions/include', 'search'); 

	remove_action('wp_head', 'rsd_link');
	remove_action('wp_head', 'wp_generator');
	remove_action('wp_head', 'feed_links', 2);
	remove_action('wp_head', 'index_rel_link');
	remove_action('wp_head', 'wlwmanifest_link');
	remove_action('wp_head', 'feed_links_extra', 3);
	remove_action('wp_head', 'start_post_rel_link', 10, 0);
	remove_action('wp_head', 'parent_post_rel_link', 10, 0);
	remove_action('wp_head', 'adjacent_posts_rel_link', 10, 0);

	add_filter('acf/settings/path', 'my_acf_settings_path');
	function my_acf_settings_path( $path ) {
	 
	    // update path
	    $path = get_stylesheet_directory() . '/acf/';
	    
	    // return
	    return $path;
	    
	}
	add_filter('acf/settings/dir', 'my_acf_settings_dir');
	function my_acf_settings_dir( $dir ) {
	    $dir = get_stylesheet_directory_uri() . '/acf/';
	    return $dir;
	}
	include_once( get_stylesheet_directory() . '/acf/acf.php' );
	add_filter('acf/settings/google_api_key', function () {
	    return 'AIzaSyBR5bX6m_CEPwitun65XjrFWYZVRtzqADA';
	});

	function wpb_set_post_views($postID) {
	    $count_key = 'wpb_post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        $count = 0;
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	    } else {
	        $count++;
	        update_post_meta($postID, $count_key, $count);
	    }
	}
	//To keep the count accurate, lets get rid of prefetching
	remove_action( 'wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
	function wpb_track_post_views ($post_id) {
	    if ( !is_single() ) return;
	    if ( empty ( $post_id) ) {
	        global $post;
	        $post_id = $post->ID;    
	    }
	    wpb_set_post_views($post_id);
	}
	add_action( 'wp_head', 'wpb_track_post_views');

	function wpb_get_post_views($postID){
	    $count_key = 'wpb_post_views_count';
	    $count = get_post_meta($postID, $count_key, true);
	    if($count==''){
	        delete_post_meta($postID, $count_key);
	        add_post_meta($postID, $count_key, '0');
	        return "0 View";
	    }
	    return $count.' Views';
	}

?>
