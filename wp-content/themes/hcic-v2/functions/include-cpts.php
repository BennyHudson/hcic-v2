<?php
	function wedo_posts() {
		register_post_type(
			'research', 
			array(
				'labels'		=> array (
									'name'					=> _x('Research', 'post type general name'),
									'singular_name'			=> _x('Research', 'post type singular name'),
									'add_new'				=> _x('Add new Research', 'book'),
									'add_new_item'			=> ('Add New Research'),
									'edit_item'				=> ('Edit Research'),
									'new_item'				=> ('New Research'),
									'all_items'				=> ('All Research'),
									'view_item'				=> ('View Research'),
									'search_items'			=> ('Search Research'),
									'not_found'				=> ('No Research found'),
									'not_found_in_trash'	=> ('No Research found in trash'),
									'parent_item_colon'		=> '',
									'menu_name'				=> 'Research'
								),
				'description'	=> 'Holds all the Research',
				'public'		=> true,
				'menu_position'	=> 19,
				'menu_icon'		=> 'dashicons-analytics',
				'supports'		=> array('title', 'editor', 'excerpt', 'revisions', 'thumbnail'),
				'has_archive'	=> true
			)
		);
	}
	function wedo_taxonomies() {
		register_taxonomy( 
			'research-category', 
			'research', 
			array(
	            'labels' 		=> array(
						 			'name'              => _x( 'Category', 'taxonomy general name' ),
						            'singular_name'     => _x( 'Category', 'taxonomy singular name' ),
						            'search_items'      => __( 'Search Categories' ),
						            'all_items'         => __( 'All Categories' ),
						            'parent_item'       => __( 'Parent Category' ),
						            'parent_item_colon' => __( 'Parent Category:' ),
						            'edit_item'         => __( 'Edit Category' ), 
						            'update_item'       => __( 'Update Category' ),
						            'add_new_item'      => __( 'Add New Category' ),
						            'new_item_name'     => __( 'New Category' ),
						            'menu_name'         => __( 'Categories' ),
						        ),
	            'hierarchical' => true,
	        )
		);
    }
?>
