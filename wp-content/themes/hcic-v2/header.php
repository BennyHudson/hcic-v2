<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
<link rel="stylesheet" href="<?php bloginfo('stylesheet_url'); ?>" type="text/css" media="screen" />
<meta charset="<?php bloginfo('charset'); ?>">
<meta name="viewport" content="width=device-width"/>  
<meta http-equiv="X-UA-Compatible" content="IE=edge" />
<?php get_template_part('includes/include', 'favicon'); ?>
<title><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
<script type="text/javascript">
    window.cookieconsent_options = {"message":"This website uses cookies to ensure you get the best experience on our website","dismiss":"Got it!","learnMore":"More info","link":null,"theme":"dark-bottom"};
</script>
<script type="text/javascript" src="//cdnjs.cloudflare.com/ajax/libs/cookieconsent2/1.0.9/cookieconsent.min.js"></script>
</head>
<body <?php body_class('wedo'); ?>>

	<section class="search-block">
		<section class="container">
			<?php dynamic_sidebar( 'quick-search' ); ?>
		</section>
	</section>

	<section class="page-content">
		<section class="footer-fix">
			<?php get_template_part('snippets/nav', 'mobile'); ?>
			<header>
				<section class="header-meta">
					<section class="container">
						<ul class="header-contact">
							<li><i class="fa fa-phone"></i> 0118 378 3896</li>
							<li><a href="mailto:t.noronha@reading.ac.uk"><i class="fa fa-envelope"></i></a></li>
							<li><a href="https://twitter.com/UniRdg_HCIC" target="_blank"><i class="fa fa-twitter"></i></a></li>
							<li><a href="#" class="search-trigger"><i class="fa fa-search"></i></a></li>
						</ul>
					</section>
				</section>
				<section class="header-main">
					<section class="header-container">
						<aside class="header-logo">
							<?php get_template_part('snippets/content', 'logo'); ?>
						</aside>
						<nav>
							<?php wp_nav_menu( array( 'theme_location' => 'header-menu' ) ); ?>
						</nav>
						<aside class="header-logo">
							<h2 class="secondary-logo"><a href="http://reading.ac.uk" target="_blank">University of Reading</a></h2>
						</aside>
					</section>
				</section>
			</header>
