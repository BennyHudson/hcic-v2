<?php if(get_field('page_sections')) { ?>

	<?php while(the_repeater_field('page_sections')) { ?>
		<?php 
			$section_width = get_sub_field('width');
			$columns = get_sub_field('columns');
		?>
		<section class="page-section <?php echo $section_width; ?>"> 
			<section class="container ultra no-top">
				<section class="cols-<?php echo $columns; ?>">
					<?php if(get_sub_field('column_1_content')) { ?>
						<aside>
							<?php 
								if(have_rows('column_1_content')) {
									$count = 0;
									while(have_rows('column_1_content')) {
										$count++;
										the_row();
										$layout = get_row_layout();
										echo '<div class="column-row '. $layout .' ">';
											get_template_part('includes/partial', 'content');
										echo '</div>';
									}
								} 
							?>
						</aside>
					<?php } ?>
					<?php if(get_sub_field('column_2_content')) { ?>
						<aside>
							<?php 
								if(have_rows('column_2_content')) {
									$count = 0;
									while(have_rows('column_2_content')) {
										$count++;
										the_row();
										$layout = get_row_layout();
										echo '<div class="column-row '. $layout .' ">';
											get_template_part('includes/partial', 'content');
										echo '</div>';
									}
								} 
							?>
						</aside>
					<?php } ?>
					<?php if(get_sub_field('column_3_content')) { ?>
						<aside>
							<?php 
								if(have_rows('column_3_content')) {
									$count = 0;
									while(have_rows('column_3_content')) {
										$count++;
										the_row();
										$layout = get_row_layout();
										echo '<div class="column-row '. $layout .' ">';
											get_template_part('includes/partial', 'content');
										echo '</div>';
									}
								} 
							?>
						</aside>
					<?php } ?>
					<?php if(get_sub_field('column_4_content')) { ?>
						<aside>
							<?php 
								if(have_rows('column_4_content')) {
									$count = 0;
									while(have_rows('column_4_content')) {
										$count++;
										the_row();
										$layout = get_row_layout();
										echo '<div class="column-row '. $layout .' ">';
											get_template_part('includes/partial', 'content');
										echo '</div>';
									}
								} 
							?>
						</aside>
					<?php } ?>
				</section>
			</section>
		</section>
	<?php } ?>

<?php } ?>
