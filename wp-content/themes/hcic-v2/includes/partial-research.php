<?php
	$terms = get_the_terms( $post->ID , 'research-category' );
?>
<li class="mix all <?php foreach( $terms as $term ) { echo ''. $term->slug . ' '; } ?>">
	<a href="<?php the_permalink(); ?>">
		<div class="research-overlay">
			<h2><?php the_title(); ?></h2>
		</div>
		<?php the_post_thumbnail('square'); ?>
	</a>
</li>
