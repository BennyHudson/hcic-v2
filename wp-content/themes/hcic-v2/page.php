<?php get_header(); ?>

	<?php 
		get_template_part('includes/content', 'builder'); 
		if(is_front_page()) {
			get_template_part('snippets/get', 'post');
		}
	?>

<?php get_footer(); ?>
