<div class="widget">
	<?php if(get_post_type() == 'research') { ?>
		<h2 class="widget-title">Recent Articles</h2>
		<ul class="widget-list">
	<?php } else { ?>
		<h2 class="widget-title">Popular News Items</h2>
		<ul class="widget-list popular-posts">
	<?php } ?>
		<?php if(get_post_type() == 'research') { ?>
			<?php 
			    $args = array(
			        'post_type'             => 'research',
			        'posts_per_page'        => 5
			    );  
			    $the_query = new WP_Query( $args );
			?>
			<?php while($the_query->have_posts()) { ?>
				<?php $the_query->the_post(); ?>
					<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?></a></li>  
		    	<?php wp_reset_postdata(); ?>
			<?php } ?>
		<?php } else { ?>
			<?php 
				$popularpost = new WP_Query( 
					array( 
						'posts_per_page' => 3, 
						'meta_key' => 'wpb_post_views_count', 
						'orderby' => 'meta_value_num', 
						'order' => 'DESC'  
					) 
				);
			?>
			<?php while ( $popularpost->have_posts() ) { ?>
				<?php $popularpost->the_post(); ?>
					<li><a href="<?php the_permalink(); ?>"><?php the_title(); ?><span class="post-count"><?php echo wpb_get_post_views(get_the_ID()); ?></span></a></li>
				<?php wp_reset_postdata(); ?>
			<?php } ?>
		<?php } ?>
	</ul>
</div>
<div class="widget">
	<h2 class="widget-title">Categories</h2>
	<ul class="widget-list">
		<?php if(get_post_type() == 'research') { ?>
			<?php 
				$taxonomy = 'research-category';
        		$tax_terms = get_terms($taxonomy);
			?>
			<?php foreach ($tax_terms as $term) { ?>
        		<li>
        			<a href="<?php bloginfo('url'); ?>/research?research-category=<?php echo $term->slug; ?>"><?php echo $term->name; ?></a>
        		</li>
			<?php } ?>
		<?php } else { ?>
			<?php 
				$taxonomy = 'category';
        		$tax_terms = get_terms($taxonomy);
			?>
			<?php foreach ($tax_terms as $term) { ?>
        		<li>
        			<a href="<?php bloginfo('url'); ?>/category/<?php echo $term->slug; ?>"><?php echo $term->name; ?></a>
        		</li>
			<?php } ?>
		<?php } ?>
	</ul>
</div>
